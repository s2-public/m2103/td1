import java.util.Scanner;

public class code3 {

	public static void main(String[] args) {
		//lire l'entier pour le code du photocopieur
		Scanner input = new Scanner(System.in);
		final int TAILLE = 4;
	    int[] code = new int[TAILLE];

	    for (int i = 0; i < code.length; i++)
	    {
	        System.out.println("Entrez un chiffre");
	        code[i] = input.nextInt();
	    }
		input.close();
		//examiner si le code du photocopieur est correct
		int somme3PremiersChiffre= code[0] + code[1] + code[2];
		if (code[3] == somme3PremiersChiffre%7) {
			//code correct
			System.out.println("Le code "+code[0]+code[1]+code[2]+code[3]+" est correct");
		}else {
			System.out.println("Le code "+code[0]+code[1]+code[2]+code[3]+" n'est pas correct");
		}
	}

}
