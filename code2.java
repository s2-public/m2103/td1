import java.util.Scanner;

public class code2 {

	public static void main(String[] args) {
		//lire l'entier pour le code du photocopieur
		Scanner entree = new Scanner(System.in);
		System.out.println("Code ?");
		String code = entree.nextLine();
		entree.close();
		//examiner si le code du photocopieur est correct
		int somme3PremiersChiffre= code.charAt(0)-'0' + code.charAt(1)-'0' + code.charAt(2)-'0';
		if (code.charAt(3)-'0' == somme3PremiersChiffre%7) {
			//code correct
			System.out.println("Le code "+code+" est correct");
		}else {
			System.out.println("Le code "+code+" n'est pas correct");
		}
	}

}
